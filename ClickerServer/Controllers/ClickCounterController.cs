﻿using ClickerServer.DataSource;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace ClickerServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClickCounterController : ControllerBase
    {
        private readonly UserContext userContext;

        public ClickCounterController(UserContext userContext)
        {
            this.userContext = userContext;
        }

        [Route("test")]
        [HttpGet]
        public string Test([FromServices] ApplicationDbContext dbContext)
        {
            var users = dbContext.Users.ToList();

            return "Logins from DB: \n" + string.Join("\n", users.Select(v => v.Login));
        }

       
        [Route("add/{count}")]
        [HttpGet]
        public long Get([FromRoute]int count)
        {
            return userContext.AddClicks(count);
        }

        [HttpGet]
        public UserGameInfo GetUserGameInfo()
        {
            return userContext.GetUserGameInfo();
        }

        [HttpPost]
        public void Update([FromBody] UserGameInfo userGameInfo)
        {
            userContext.UpdateUserAction(userGameInfo);
        }
    }
}
