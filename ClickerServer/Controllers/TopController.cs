﻿using ClickerServer.DataSource;
using ClickerServer.DataSource.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ClickerServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TopController : ControllerBase
    {
        public TopController()
        {
        }

        [HttpGet]
        public UserClicks[] GetTop([FromServices]ApplicationDbContext dbContext, [FromQuery]int start, [FromQuery]int count)
        {
            var records = dbContext.Users
                .Select(v => new UserClicks() { UserIdent = v.Login, ClicksCount = v.ClickCount })
                .OrderByDescending(v => v.ClicksCount)
                .Skip(start)
                .Take(count);
                //.ToArray();

            return records.ToArray();
        } 
    }
}
