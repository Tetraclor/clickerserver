﻿using ClickerServer.DataSource;
using ClickerServer.DataSource.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClickerServer.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        [HttpGet]
        [Route("myreferals")]
        public UserClicks[] GetReferal([FromServices] UserContext userContext)
        {
            return userContext.GetReferalClicks();
        }

        [HttpGet]
        [Route("referalkey")]
        public string GetReferalKey([FromServices]UserContext userContext)
        {
            return userContext.User.ReferalKey;
        }
    }
}
