﻿using ClickerServer.DataSource.Models;
using System;
using System.Linq;

namespace ClickerServer.DataSource
{
    public interface IUserAccount
    {
        bool UpdateUserContext(string token, UserContext userContext);
        string GetTokenOrNull(string login);
        bool Register(string login, string password, string referalKey);
        bool LogIn(string login, string password);

        string Description { get; }
    }

    public class UserAccountService : IUserAccount
    {
        ApplicationDbContext _dbContext;

        public string Description { get; private set; }

        public UserAccountService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool UpdateUserContext(string token, UserContext userContext)
        {
            var rowToken = _dbContext.Tokens.FirstOrDefault(v => v.TokenValue == token);
            if (rowToken == null) return false;

            return userContext.SetUser(rowToken.UserId);
        }

        public string GetTokenOrNull(string login)
        {
            var user = _dbContext.Users.FirstOrDefault(v => v.Login == login);
            if (user == null) return null;

            var token = _dbContext.Tokens.Find(user.UserId);
            if (token == null)
            {
                //По какой-то причене не создался токен при регистрации. Или удалился после.
                //Создаем новый
                _dbContext.Add(new Token()
                {
                    TokenValue = CreateToken(),
                    UserId = user.UserId
                });
                _dbContext.SaveChanges();
            }

            token = _dbContext.Tokens.Find(user.UserId);

            return token.TokenValue;
        }

        public bool LogIn(string login, string password)
        {
            var user = _dbContext.Users.FirstOrDefault(v => v.Login == login);
            if (user == null) return false;

            if(user.ReferalKey == null) 
            {
                user.ReferalKey = CreateReferalKey();
                _dbContext.SaveChanges();
            }

            return user.Password == password; // Аж самому смешно
        }

        public bool Register(string login, string password, string referalKey)
        {
            var user = _dbContext.Users.FirstOrDefault(v => v.Login == login);
            if (user != null) 
            {
                Description = "Пользователь с данным логином уже зарегестрирован";
                return false; // Пользователь с таким логином уже зарегестрирован.
            }

            user = new User() {
                Login = login,
                Password = password,
                ReferalKey = CreateReferalKey()
            };

            // TODO оптимизировать, чтобы за одно сохранение
            _dbContext.Database.BeginTransaction();

            _dbContext.Add(user); 
            _dbContext.SaveChanges(); // Записываем пользователя в БД, таким образом у пользователя появится UserId

            _dbContext.Add(new Token() {
                TokenValue = CreateToken(), 
                UserId = user.UserId 
            });

            if(referalKey != null || referalKey != "")
            {
                var referalUser = _dbContext.Users.FirstOrDefault(v => v.ReferalKey == referalKey);

                if (referalUser == null)
                {
                    Description = "Не найден пользователь с данным реферальным кодом";
                    return false;
                }

                _dbContext.Add(new Referal()
                {
                    UserId = user.UserId,
                    ReferalUserId = referalUser.UserId
                });
            }

            _dbContext.SaveChanges();

            _dbContext.Database.CommitTransaction();

            return true;
        }

        private string CreateToken()
        {
            // TODO сделать нормальную генерацию токена.
            var random = new Random();
            return random.NextDouble().ToString();
        }

        private string CreateReferalKey()
        {
            // TODO сделать нормальную генерацию реферального ключа
            var random = new Random();
            return random.NextDouble().ToString().Substring(2, 6);
        }
    }
}
