﻿using ClickerServer.DataSource.Models;
using Microsoft.EntityFrameworkCore;

namespace ClickerServer.DataSource
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<Referal> Referals { get; set; }
        public DbSet<Resources> Resources { get; set; }
        public DbSet<StorageResources> StorageResources { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }
    }
}
