﻿namespace ClickerServer.DataSource.DTO
{
    public class UserClicks
    {
        public string UserIdent { get; set; }
        public int ClicksCount { get; set; }
    }
    
}
