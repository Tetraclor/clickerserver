﻿using ClickerServer.DataSource.DTO;
using ClickerServer.DataSource.Models;
using System.Collections.Generic;
using System.Linq;

namespace ClickerServer.DataSource
{
    public partial class UserContext
    {
        private readonly ApplicationDbContext dbContext;
        private bool userSet = false;

        public UserContext(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public User User { get; private set; }

        public bool SetUser(int userId)
        {
            if (userSet) return false; // Чтобы можно было назначить пользователя только один раз.
            User = dbContext.Users.Find(userId);
            return User != null;
        }

        // TODO методы достающие из и обновляющие БД данные конкретного пользователя.
        public int AddClicks(int count)
        {
            User.ClickCount += count;
            dbContext.SaveChanges(); // Сохранять сразу в бд сразу или в конце запроса? 
            return User.ClickCount;
        }

        public void UpdateUserAction(UserGameInfo userActionsInfo)
        {
            AddClicks(userActionsInfo.CountClicks);

            var userResources = dbContext.StorageResources.Where(v => v.UserId == User.UserId);

            foreach (var res in userActionsInfo.MineResources)
            {
                var resourceRow = userResources.FirstOrDefault(v => v.ResourceId == res.ResourceId);
                if(resourceRow == null)
                {
                    dbContext.StorageResources.Add(new StorageResources() { ResourceId = res.ResourceId, Count = res.Count});
                }
                else
                {
                    resourceRow.Count += res.Count;
                }
            }
            dbContext.SaveChanges();
        }

        public UserGameInfo GetUserGameInfo()
        {
            var userResources = dbContext.StorageResources
                .Where(v => v.UserId == User.UserId)
                .Select(v => new ResourceCount() { Count = v.Count, ResourceId = v.ResourceId})
                .ToArray();

            return new UserGameInfo()
            {
                CountClicks = User.ClickCount,
                MineResources = userResources
            };
        }

        public UserClicks[] GetReferalClicks()
        {
            var userId = User.UserId;
            var result = dbContext.Referals
                .Where(v => v.UserId == userId)
                .Join(dbContext.Users, 
                    a => a.ReferalUserId, 
                    b => b.UserId, 
                    (a, b) => new UserClicks() { 
                        UserIdent = b.Login,
                        ClicksCount = b.ClickCount
                    }
                ).ToArray();

            return result;
        }
    }

    // DTO
    public class ResourceCount
    {
        public int ResourceId { get; set; }
        public long Count { get; set; }
    }

    public class UserGameInfo
    {
        public int CountClicks { get; set; }
        public ResourceCount[] MineResources { get; set; }
    }
}
