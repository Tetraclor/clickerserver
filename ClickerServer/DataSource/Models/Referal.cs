﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClickerServer.DataSource.Models
{
    public class Referal
    {
        [Key]
        public int RowId { get; set; }
        public int UserId { get; set; }
        public int ReferalUserId { get; set; }
    }
}
