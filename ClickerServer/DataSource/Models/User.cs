﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace ClickerServer.DataSource.Models
{
    public class User
    {
        [Key]
        public int UserId { get; private set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int ClickCount { get; set; }
        public string ReferalKey { get; set; }
    }
}
