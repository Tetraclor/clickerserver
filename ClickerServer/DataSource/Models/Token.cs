﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace ClickerServer.DataSource.Models
{
    public class Token
    {
        [Key]
        public int UserId { get; set; }
        [Column("Token")]
        public string TokenValue { get; set; }
    }
}
