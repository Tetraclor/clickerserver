﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClickerServer.DataSource.Models
{
    public class Resources
    {
        [Key]
        public int ResourceId { get; set; }
        public string Name { get; set; }
    }

    public class StorageResources
    {
        [Key]
        public int UserId { get; set; }
        public int ResourceId { get; set; }
        public long Count { get; set; }
    }
}
