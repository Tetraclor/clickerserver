﻿using ClickerServer.DataSource;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ClickerServer.Middleware
{
    public class TokenMiddleware  
    {
        private readonly RequestDelegate next;

        public TokenMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context, IUserAccount userAccount, UserContext userContext)
        {
            var token = context.Request.Query["token"];
            
            if (userAccount.UpdateUserContext(token, userContext) == false)
            {
                context.Response.StatusCode = 403;
                await context.Response.WriteAsync("Token is invalid");
            }
            else
            {
                await next.Invoke(context); // Токен действителен, продолжить выполнение конвеера.
            }
        }
    }
}
