﻿using ClickerServer.DataSource;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace ClickerServer.Middleware
{
    public class RegisterMiddleware 
    {
        private readonly RequestDelegate next;

        public RegisterMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context, IUserAccount userAccount)
        {
            var login = context.Request.Query["login"];
            var password = context.Request.Query["password"];
            var referalKey = context.Request.Query["referal_key"];

            if (login.Count == 0 || password.Count == 0)
            {
                await Fail(context, "Login or password is empty");
                return;
            }

            if (userAccount.Register(login, password, referalKey) == false)
            {
                await Fail(context, userAccount.Description);
                return;
            }

            var token = userAccount.GetTokenOrNull(login);
            await context.Response.WriteAsync(token);
        }

        private Task Fail(HttpContext context, string message)
        {
            context.Response.StatusCode = 400;
            return  context.Response.WriteAsync(message);
        }
    }
}
