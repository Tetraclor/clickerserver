﻿using ClickerServer.DataSource;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace ClickerServer.Middleware
{
    public class LoginMiddleware
    {
        private readonly RequestDelegate next;

        public LoginMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context, IUserAccount userAccount)
        {
            var login = context.Request.Query["login"];
            var password = context.Request.Query["password"];

            if (login.Count == 0 || password.Count == 0)
            {
                context.Response.StatusCode = 403;
                await context.Response.WriteAsync("Login or password is empty");
                return;
            }

            if (userAccount.LogIn(login, password) == false)
            {
                context.Response.StatusCode = 403;
                await context.Response.WriteAsync("Login or password is invalid");
                return;
            }

            var token = userAccount.GetTokenOrNull(login);
            await context.Response.WriteAsync(token);
            return;
        }
    }
}
