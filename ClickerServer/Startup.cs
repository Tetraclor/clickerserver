using ClickerServer.DataSource;
using ClickerServer.Middleware;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace ClickerServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // �������� ������ ����������� �� ����� ������������
            // TODO ���������, ��� ������������� ����������� ������ �����������, ����� �������� �� ����� � ��������
            string connection = Configuration.GetConnectionString("DefaultConnection");
            // string connection = Configuration.GetConnectionString("HostConnection");

            // ��������� �������� ApplicationContext � �������� ������� � ����������
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connection));

            services.AddScoped<IUserAccount, UserAccountService>(); 
            services.AddScoped<UserContext>(); // Scoped - ���� ������ �� ���� ������.

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            // Login -> ������ �����
            // Register -> ������� � ������ �����
            app.Map("/login", b => b.UseMiddleware<LoginMiddleware>());
            app.Map("/register", b => b.UseMiddleware<RegisterMiddleware>());

            // �������� ������
            app.UseMiddleware<TokenMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
